<?php

class AuthenticationController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return mixed
	 */
	public function index()
	{
		return View::make('login')->with('title','User login');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return mixed
	 */
	public function login()
	{
		try{
			$credentials = array(
				'email' 	=> Input::get('email'),
				'password' 	=> Input::get('password')
				//'password' 	=> Hash::make(Input::get('password'))
			);

			$user = Sentry::authenticate($credentials, false);
			if($user){
				return Redirect::to('dashboard');
			}
		}

		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
		    Session::flash('flash_message', 'Login field is required');
			return Redirect::to('/')->withErrors('login_errors',$e->getMessage())->with('title','Login errors');
		}
		catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
		{
		    Session::flash('flash_message', 'Password field is required');
			return Redirect::to('/')->withErrors('login_errors',$e->getMessage())->with('title','Login errors');
		}
		catch (Cartalyst\Sentry\Users\WrongPasswordException $e)
		{
		    Session::flash('flash_message', 'Wrong password, try again');
			return Redirect::to('/')->withErrors('login_errors',$e->getMessage())->with('title','Login errors');
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    Session::flash('flash_message', 'User was not found');
			return Redirect::to('/')->withErrors('login_errors',$e->getMessage())->with('title','Login errors');
		}
		catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
		{
		    Session::flash('flash_message', 'User is not activated');
			return Redirect::to('/')->withErrors('login_errors',$e->getMessage())->with('title','Login errors');
		}

		// The following is only required if the throttling is enabled
		catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
		{
		    Session::flash('flash_message', 'User is suspended');
			return Redirect::to('/')->withErrors('login_errors',$e->getMessage())->with('title','Login errors');
		}
		catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
		{
		    Session::flash('flash_message', 'User is banned');
			return Redirect::to('/')->withErrors('login_errors',$e->getMessage())->with('title','Login errors');
		}
		catch(Exception $e){
			Session::flash('flash_message', 'Don\'t try to direct access');
			return Redirect::to('/')->with('title','Login errors');
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// TODO create resource

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return mixed
	 */
	public function password()
	{
		return View::make('backend.change_user_password')->with('title', 'Change user password');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return mixed
	 */
	public function changePassword()
	{
		$input = [
			'user_id' => Input::get('user_id'),
			'password' 		=> Input::get('password'),
			'confirm_pass' 	=> Input::get('confirm_pass')
		];

		$rules = [
			'user_id'     	=> 'required|numeric|min:1',
			'password'     	=> 'required|alpha_num|min:10|max:32',
            'confirm_pass'  => 'required|same:password',
		];

		try
		{
			// call validation class
    		$validation = Validator::make($input, $rules);

			if($validation->fails())
			{
				return Redirect::route("pass")->withErrors($validation)->withInput();
			}
			else{
				// Find the user using the user id
			    $user = Sentry::findUserById($input['user_id']);
			    // Update the user details
			    $user->password = $input['password'];
			    // if updates the user
			    $updated = $user->save();

			    if ($updated)
			    {
			    	Session::flash('success_message', "Password is updated just now."); // set the flash message
            		return Redirect::route("pass");
			    }
			    else
			    {
			        Session::flash('flash_message', "Something is going to wrong. Please try again later!"); // set the flash message
            		return Redirect::route("pass");
			    }
			}
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    Session::flash('flash_message', $input['first_name']."'s"." was not found."); // set the flash message
            return Redirect::route("pass");
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return mixed
	 */
	public function editProfile()
	{
		return View::make('backend.edit_user_profile')->with('title', 'Edit user profile');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return mixed
	 */
	public function updateProfile()
	{
		$input = [
			'user_id' => Input::get('user_id'),
			'email' => Input::get('email'),
			'first_name' => Input::get('first_name'),
			'last_name' => Input::get('last_name')
		];

		$rules = [
			'user_id'     	=> 'required|numeric|min:1',
            'email'         => 'required|email',
            'first_name'    => 'required',
            'last_name'     => 'required'
		];
		

		try
		{
			// call validation class
    		$validation = Validator::make($input, $rules);

			if($validation->fails()){
				return Redirect::route("profile_edit")->withErrors($validation)->withInput();
			}
			else{
			    // Find the user using the user id
			    $user = Sentry::findUserById($input['user_id']);
			    // Update the user details
			    $user->email = $input['email'];
			    $user->first_name = $input['first_name'];
			    $user->last_name = $input['last_name'];
			    $updated = $user->save();

			    // if updates the user
			    if ($updated)
			    {
			    	Session::flash('success_message', $input['first_name']."'s"." information is updated just now."); // set the flash message
            		return Redirect::route("profile_edit");
			    }
			    else
			    {
			        Session::flash('flash_message', "Something is going to wrong. Please try again later!"); // set the flash message
            		return Redirect::route("profile_edit");
			    }
			}
		}

		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    Session::flash('flash_message', $input['first_name']."'s"." was not found."); // set the flash message
            return Redirect::route("profile_edit");
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return mixed
	 */
	public function logout()
	{
		Sentry::logout();
		return Redirect::to('/');
	}
}
